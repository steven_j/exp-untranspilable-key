# exp-untranspilable-key

Just run `make test` to see the error:

```sh
 make test

untranspilable key 0x0062c151b58b2af0063316beb54f86a536df060a19c123e41ed080d18a6bf10260
```

The error seems related to the test framework, compilation is ok, as is `compile expression`:

```sh
ligo compile expression cameligo 'Big_map.literal [(1n, ("edpkvYqwmqNiRdzHKxbdr3iuV7aduW4DcckgdaTtAbXdBu5Gu5hpur" : key))]'
{ Elt 1 "edpkvYqwmqNiRdzHKxbdr3iuV7aduW4DcckgdaTtAbXdBu5Gu5hpur" }
```
