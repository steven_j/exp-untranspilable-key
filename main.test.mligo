#import "./main.mligo" "MyContract"

let (_, pub_key, secret_key) = Test.get_bootstrap_account 1n

let (taddr, _, _) =
  Test.originate
    MyContract.main
    {registry = (Big_map.empty : MyContract.registry);
     next_id = 1n}
    0mutez

let contr = Test.to_contract taddr

let test_log_pub_key = Test.log(pub_key)

let test_bug =
  Test.transfer_to_contract contr pub_key 0mutez
