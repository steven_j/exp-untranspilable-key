SHELL := /bin/bash

LIGO=ligo
ifeq (, $(shell which ligo))
	LIGO=@docker run -v "$(PWD):$(PWD)" -w "$(PWD)" --rm -i ligolang/ligo:next
endif

help:
	@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | \
	awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

test: ## run tests
	@$(LIGO) run test main.test.mligo

compile: ## compile contracts
	@if [ ! -d ./compiled ]; then mkdir -p ./compiled ; fi
	@$(LIGO) compile contract main.mligo -o ./compiled/mycontract.tz

clean: ## clean up
	@rm -rf compiled
