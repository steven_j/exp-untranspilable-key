let no_operation : operation list = []

type id = nat

type registry = (id, key) big_map

type storage = {registry : registry; next_id : nat}

type result = operation list * storage

let create (pub_key, s : key * storage) : storage =
  {s with
    next_id = s.next_id + 1n;
    registry = Big_map.add s.next_id pub_key s.registry}

let main (p, store : key * storage) : result =
  no_operation, create (p, store)
